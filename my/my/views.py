from django.http import HttpResponse
from django.template import Template, Context
from django.shortcuts import render
from django.template.loader import get_template
from commanders.models import Commander

def about(request):
    name = 'Vlad Chistov'
    return render(request, 'about.html', {'name': name})

def main(request):
    return render(request, 'main.html', {})

def com(request):
    return render(request, 'com.html', {})

def ops(request):
    return render(request, 'ops.html', {})

def egy_att(request):
    return render(request, 'egy_att.html', {})

def lyb_op(request):
    return render(request, 'lyb_op.html', {})

def rom_first(request):
    return render(request, 'rom_first.html', {})

def na_op(request):
    return render(request, 'na_op.html', {})

def rom_sec(request):
    return render(request, 'rom_sec.html', {})

def alam(request):
    return render(request, 'alam.html', {})

def r(request):
    error = False
    books = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            error = True
        elif q == "*":
            books = Commander.objects.all()
        else:
            if 'reverse' in request.GET:
                reverse = request.GET['reverse']
                books = Commander.objects.filter(name=q.capitalize)
            else:
                books = Commander.objects.filter(name__contains=q.capitalize)
    return render(request, '123.html', {'error': error, 'commander': books})