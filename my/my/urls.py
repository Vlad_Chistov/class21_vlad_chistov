"""my URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from my.views import about, main, com, ops, r, egy_att, lyb_op, rom_first, na_op, rom_sec, alam


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^about/$', about, name='about'),
    url(r'^operations/$', ops, name='operations'),
    url(r'^operations/egypt_attack$', egy_att, name='egy_att'),
    url(r'^operations/lybian_operation', lyb_op, name='lyb_op'),
    url(r'^operations/rommel_first_attack$', rom_first, name='rom_first'),
    url(r'^operations/northafrican_operation$', na_op, name='na_op'),
    url(r'^operations/rommel_second_attack$', rom_sec, name='rom_sec'),
    url(r'^operations/el_alamein$', alam, name='alam'),
    url(r'^$', main, name='main'),
    url(r'^commanders/', include('commanders.urls')),
    url(r'^search/$', r, name='search'),


]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
