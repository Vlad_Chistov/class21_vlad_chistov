# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('commanders', '0002_remove_commander_photo'),
    ]

    operations = [
        migrations.AddField(
            model_name='commander',
            name='photo',
            field=models.FileField(upload_to='', blank=True, null=True),
        ),
    ]
