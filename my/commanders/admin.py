from django.contrib import admin
from .models import Commander, Country, Side

admin.site.register(Commander)
admin.site.register(Country)
admin.site.register(Side)
