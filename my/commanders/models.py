from django.db import models

class Side(models.Model):
    side = models.CharField(max_length=30)

    def __str__(self):
        return self.side

class Country(models.Model):
    name = models.CharField(max_length=30)
    side = models.ForeignKey(Side)

    def __str__(self):
        return self.name

class Commander(models.Model):
    name = models.CharField(max_length=50)
    country = models.ForeignKey(Country)
    photo = models.FileField(blank=True, null=True)
    info = models.TextField()

    def __str__(self):
        return self.name
