from django.shortcuts import render
from commanders.models import Commander

def comm(request):
    rom = Commander.objects.get(name='Эрвин Роммель')
    arn = Commander.objects.get(name='Ганс-Юрген фон Арнем')
    bal = Commander.objects.get(name='Итало Бальбо')
    bas = Commander.objects.get(name='Этторе Бастико')
    eis = Commander.objects.get(name='Дуайт Эйзенхауэр')
    mon = Commander.objects.get(name='Бернард Монтгомерри')
    wav = Commander.objects.get(name='Арчибальд Уэйвелл')
    return render(request, 'comm.html', {'rom': rom, 'arn': arn, 'bal': bal, 'bas': bas,
                                         'eis': eis, 'mon': mon, 'wav': wav})

def rom(request):
    rom = Commander.objects.get(name='Эрвин Роммель')
    arn = Commander.objects.get(name='Ганс-Юрген фон Арнем')
    bal = Commander.objects.get(name='Итало Бальбо')
    bas = Commander.objects.get(name='Этторе Бастико')
    eis = Commander.objects.get(name='Дуайт Эйзенхауэр')
    mon = Commander.objects.get(name='Бернард Монтгомерри')
    wav = Commander.objects.get(name='Арчибальд Уэйвелл')
    return render(request, 'rom.html', {'rom': rom, 'arn': arn, 'bal': bal, 'bas': bas,
                                         'eis': eis, 'mon': mon, 'wav': wav, 'com': rom})

def arn(request):
    rom = Commander.objects.get(name='Эрвин Роммель')
    arn = Commander.objects.get(name='Ганс-Юрген фон Арнем')
    bal = Commander.objects.get(name='Итало Бальбо')
    bas = Commander.objects.get(name='Этторе Бастико')
    eis = Commander.objects.get(name='Дуайт Эйзенхауэр')
    mon = Commander.objects.get(name='Бернард Монтгомерри')
    wav = Commander.objects.get(name='Арчибальд Уэйвелл')
    return render(request, 'arn.html', {'rom': rom, 'arn': arn, 'bal': bal, 'bas': bas,
                                         'eis': eis, 'mon': mon, 'wav': wav, 'com': arn})
def bal(request):
    rom = Commander.objects.get(name='Эрвин Роммель')
    arn = Commander.objects.get(name='Ганс-Юрген фон Арнем')
    bal = Commander.objects.get(name='Итало Бальбо')
    bas = Commander.objects.get(name='Этторе Бастико')
    eis = Commander.objects.get(name='Дуайт Эйзенхауэр')
    mon = Commander.objects.get(name='Бернард Монтгомерри')
    wav = Commander.objects.get(name='Арчибальд Уэйвелл')
    return render(request, 'bal.html', {'rom': rom, 'arn': arn, 'bal': bal, 'bas': bas,
                                         'eis': eis, 'mon': mon, 'wav': wav, 'com': bal})
def bas(request):
    rom = Commander.objects.get(name='Эрвин Роммель')
    arn = Commander.objects.get(name='Ганс-Юрген фон Арнем')
    bal = Commander.objects.get(name='Итало Бальбо')
    bas = Commander.objects.get(name='Этторе Бастико')
    eis = Commander.objects.get(name='Дуайт Эйзенхауэр')
    mon = Commander.objects.get(name='Бернард Монтгомерри')
    wav = Commander.objects.get(name='Арчибальд Уэйвелл')
    return render(request, 'bas.html', {'rom': rom, 'arn': arn, 'bal': bal, 'bas': bas,
                                         'eis': eis, 'mon': mon, 'wav': wav, 'com': bas})

def eis(request):
    rom = Commander.objects.get(name='Эрвин Роммель')
    arn = Commander.objects.get(name='Ганс-Юрген фон Арнем')
    bal = Commander.objects.get(name='Итало Бальбо')
    bas = Commander.objects.get(name='Этторе Бастико')
    eis = Commander.objects.get(name='Дуайт Эйзенхауэр')
    mon = Commander.objects.get(name='Бернард Монтгомерри')
    wav = Commander.objects.get(name='Арчибальд Уэйвелл')
    return render(request, 'eis.html', {'rom': rom, 'arn': arn, 'bal': bal, 'bas': bas,
                                         'eis': eis, 'mon': mon, 'wav': wav, 'com': eis})

def mon(request):
    rom = Commander.objects.get(name='Эрвин Роммель')
    arn = Commander.objects.get(name='Ганс-Юрген фон Арнем')
    bal = Commander.objects.get(name='Итало Бальбо')
    bas = Commander.objects.get(name='Этторе Бастико')
    eis = Commander.objects.get(name='Дуайт Эйзенхауэр')
    mon = Commander.objects.get(name='Бернард Монтгомерри')
    wav = Commander.objects.get(name='Арчибальд Уэйвелл')
    return render(request, 'mon.html', {'rom': rom, 'arn': arn, 'bal': bal, 'bas': bas,
                                         'eis': eis, 'mon': mon, 'wav': wav, 'com': mon})

def wav(request):
    rom = Commander.objects.get(name='Эрвин Роммель')
    arn = Commander.objects.get(name='Ганс-Юрген фон Арнем')
    bal = Commander.objects.get(name='Итало Бальбо')
    bas = Commander.objects.get(name='Этторе Бастико')
    eis = Commander.objects.get(name='Дуайт Эйзенхауэр')
    mon = Commander.objects.get(name='Бернард Монтгомерри')
    wav = Commander.objects.get(name='Арчибальд Уэйвелл')
    return render(request, 'wav.html', {'rom': rom, 'arn': arn, 'bal': bal, 'bas': bas,
                                         'eis': eis, 'mon': mon, 'wav': wav, 'com': wav})
