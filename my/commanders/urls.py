from django.conf.urls import include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from commanders.views import comm, rom, arn, bal, bas, eis, wav, mon

urlpatterns = [
    url(r'^$', comm,  name='commanders'),
    url(r'^rommel/', rom,  name='rom'),
    url(r'^arnem/', arn,  name='arn'),
    url(r'^balbo/', bal,  name='bal'),
    url(r'^bastiko/', bas,  name='bas'),
    url(r'^eisenhauer/', eis,  name='eis'),
    url(r'^wavell/', wav,  name='wav'),
    url(r'^montgomery/', mon,  name='mon'),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)