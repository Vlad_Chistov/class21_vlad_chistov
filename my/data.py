from commanders.models import Side, Country, Commander

Side.objects.all().delete()
Country.objects.all().delete()
Commander.objects.all().delete()

side_ax = Side.objects.create(side='Страны Оси')
side_al = Side.objects.create(side='Союзники')

country_ger = Country.objects.create(name='Германия', side=side_ax)
country_ita = Country.objects.create(name='Италия', side=side_ax)
country_uk = Country.objects.create(name='Великобритания', side=side_al)
country_usa = Country.objects.create(name='США', side=side_al)

com_rom = Commander.objects.create(name='Эрвин Роммель', info='Немецкий военачальник',
                                   country=country_ger)
com_arn = Commander.objects.create(name='Ганс-Юрген фон Арнем', info='Немецкий военачальник',
                                   country=country_ger)
com_bal = Commander.objects.create(name='Итало Бальбо', info='Итальянский военачальник',
                                   country=country_ita)
com_bas = Commander.objects.create(name='Этторе Бастико', info='Итальянский военачальник',
                                   country=country_ita)
com_eis = Commander.objects.create(name='Дуайт Эйзенхауэр', info='Американский военачальник',
                                   country=country_usa)
com_mon = Commander.objects.create(name='Бернард Монтгомерри', info='Британский военачальник',
                                   country=country_uk)
com_wav = Commander.objects.create(name='Арчибальд Уэйвелл', info='Британский военачальник',
                                   country=country_uk)
